FROM golang:alpine3.17 as stage

WORKDIR /app
COPY . .
RUN env CGO_ENABLED=0 go build -tags netgo -ldflags '-w -s -extldflags "-static"' -o myserver

FROM scratch
COPY --from=stage /app/myserver .
CMD ["./myserver"]